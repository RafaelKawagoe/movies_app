import 'package:flutter/material.dart';
import 'package:my_movies_list/ui/pages/classified_page.dart';
import 'package:my_movies_list/ui/pages/home_page.dart';
import 'package:my_movies_list/ui/pages/login_page.dart';
import 'package:my_movies_list/ui/pages/registration_page.dart';
import 'package:my_movies_list/ui/pages/search_page.dart';
import 'package:my_movies_list/ui/pages/splash_page.dart';
import 'package:my_movies_list/ui/pages/title_details_page.dart';
import 'package:my_movies_list/ui/pages/users_page.dart';

import 'locator.dart';

void main() {
  setupLocator();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'my Movie List',
      debugShowCheckedModeBanner: false,
      initialRoute: LoginPage.name,
      routes: {
        LoginPage.name: (_) => LoginPage(),
        HomePage.name: (_) => const HomePage(),
        SearchPage.name: (_) => SearchPage(),
        TitleDetailsPage.name: (_) => TitleDetailsPage(),
        RegistrationPage.name: (_) => RegistrationPage(),
        ClassifiedPage.name: (_) => ClassifiedPage(),
        SplashPage.name: (_) => SplashPage(),
        UsersPage.name: (_) => UsersPage(),
      },
    );
  }
}
