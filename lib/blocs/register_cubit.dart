import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/exceptions/user_already_exists.dart';
import 'package:my_movies_list/data/exceptions/user_not_found.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';

enum RegisterState {
  initial,
  processingRegister,
  registerFailed,
  userAlreadyExist,
  success
}

class RegisterCubit extends Cubit<RegisterState> {
  final UserRepositoryInterface _userRepository;

  RegisterCubit(this._userRepository) : super(RegisterState.initial);

  Future<void> register(String name, String email, String password) async {
    emit(RegisterState.processingRegister);
    try {
      await _userRepository.register(name, email, password);
      emit(RegisterState.success);
    } on UserAlredyExistsException {
      emit(RegisterState.userAlreadyExist);
    } on Exception {
      emit(RegisterState.registerFailed);
    }
  }
}
