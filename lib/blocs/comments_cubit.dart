import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/models/title_details_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';

abstract class CommentState {
  List<CommentModel> getCommentList();
}

class InicialState extends CommentState {
  @override
  List<CommentModel> getCommentList() => [];
}

class LoadingState extends CommentState {
  @override
  List<CommentModel> getCommentList() => [];
}

class LoadedState extends CommentState {
  LoadedState(this.comments);

  final List<CommentModel> comments;

  @override
  List<CommentModel> getCommentList() => comments;
}

class ErrorState extends CommentState {
  @override
  List<CommentModel> getCommentList() => [];
}

class CommentCubit extends Cubit<CommentState> {
  final TitleRepositoryInterface _titleRepository;

  CommentCubit(this._titleRepository) : super(InicialState());

  Future<void> getList(int id, bool isTvShow) async {
    emit(LoadingState());
    try {
      var comments = await _titleRepository.getComments(id, isTvShow);
      emit(LoadedState(comments));
    } on Exception {
      emit(ErrorState());
    }
  }
}
