import 'package:flutter/material.dart';
import 'package:my_movies_list/ui/pages/classified_page.dart';
import 'package:my_movies_list/ui/pages/registration_page.dart';
import 'package:my_movies_list/ui/pages/search_page.dart';
import 'package:my_movies_list/ui/pages/tab_pages/main_tab_page.dart';
import 'package:my_movies_list/ui/pages/tab_pages/movies_tab_page.dart';
import 'package:my_movies_list/ui/pages/tab_pages/series_tab_page.dart';
import 'package:my_movies_list/ui/pages/users_page.dart';

class HomePage extends StatelessWidget {
  static const name = 'home-page';

  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Row(
            children: [
              IconButton(
                  onPressed: () {
                    Navigator.pushNamed(context, UsersPage.name);
                  },
                  icon: const Icon(Icons.account_circle)),
              const Text(
                'Meu catálogo de filmes e séries',
                style: TextStyle(fontSize: 18),
              ),
            ],
          ),
          bottom: const TabBar(
            tabs: [
              Tab(child: Text('Principal')),
              Tab(child: Text('Filmes')),
              Tab(child: Text('Series')),
            ],
          ),
          actions: [
            IconButton(
                onPressed: () {
                  Navigator.pushNamed(context, SearchPage.name);
                },
                icon: const Icon(Icons.search)),
          ],
        ),
        body: TabBarView(
          physics: const NeverScrollableScrollPhysics(),
          children: [
            MainTabPage(),
            const MoviesTabPage(),
            const SeriesTabPage(),
          ],
        ),
      ),
    );
  }
}
