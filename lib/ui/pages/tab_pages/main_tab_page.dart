import 'package:flutter/material.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/models/title_rows.model.dart';
import 'package:my_movies_list/data/repositories/title_repository.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';
import 'package:my_movies_list/ui/widgets/thumbnail.dart';
import 'package:my_movies_list/ui/widgets/title_carousel.dart';

import '../../../locator.dart';
import '../title_details_page.dart';

class MainTabPage extends StatelessWidget {
  MainTabPage({Key? key}) : super(key: key);

  final _repository = getIt.get<TitleRepositoryInterface>();

  @override
  Widget build(BuildContext context) {
    List<TitleRowModel>? rowList = [
      TitleRowModel(
          genre: 28,
          title: 'Filmes de Ação',
          context: context,
          isTvShow: false),
      TitleRowModel(
          genre: 16,
          title: 'Filmes de Animação',
          context: context,
          isTvShow: false),
      TitleRowModel(
          genre: 12,
          title: 'Filmes de Aventura',
          context: context,
          isTvShow: false),
      TitleRowModel(
          genre: 35,
          title: 'Series de Comédia',
          context: context,
          isTvShow: true),
      TitleRowModel(
          genre: 16,
          title: 'Series de Animação',
          context: context,
          isTvShow: true),
      TitleRowModel(
          genre: 80,
          title: 'Series de Crime',
          context: context,
          isTvShow: true),
    ];

    return Padding(
        padding: const EdgeInsets.all(8),
        child: ListView.builder(
          itemCount: rowList.length,
          itemBuilder: (context, index) {
            return rowList[index].isTvShow
                ? _buildSerieRow(rowList[index])
                : _buildMovieRow(rowList[index]);
          },
        ));
  }

  Widget _buildMovieRow(TitleRowModel titleRowModel) {
    return Column(
      children: [
        const SizedBox(
          height: 5,
        ),
        FutureBuilder<List<TitleModel>>(
          future:
              _repository.getMovieList(params: {'genre': titleRowModel.genre}),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Text('Carregando');
            }
            final movies = snapshot.data;
            return TitleCarousel(
              label: titleRowModel.title,
              children: movies!
                  .map((t) => _buildTitleCard(title: t, context: context))
                  .toList(),
            );
          },
        ),
      ],
    );
  }

  Widget _buildSerieRow(TitleRowModel titleRowModel) {
    return Column(
      children: [
        const SizedBox(
          height: 5,
        ),
        FutureBuilder<List<TitleModel>>(
          future: _repository.getTvList(params: {'genre': titleRowModel.genre}),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Text('Carregando');
            }
            final movies = snapshot.data;
            return TitleCarousel(
              label: titleRowModel.title,
              children: movies!
                  .map((t) => _buildTitleCard(title: t, context: context))
                  .toList(),
            );
          },
        ),
      ],
    );
  }

  Widget _buildTitleCard(
      {required TitleModel title, required BuildContext context}) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, TitleDetailsPage.name,
            arguments: {"id": title.id, "isTvShow": title.isTvShow});
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Thumbnail(
          width: 120.0,
          title: TitleThumbnailModel(
              name: title.name, imageUrl: title.posterUrl, id: title.id),
        ),
      ),
    );
  }
}
