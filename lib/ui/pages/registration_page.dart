import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/blocs/register_cubit.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/ui/widgets/loading_button.dart';
import 'package:my_movies_list/ui/widgets/text_form_field.dart';

import '../../locator.dart';
import 'home_page.dart';

class RegistrationPage extends StatelessWidget {
  static const name = 'registration-page';
  const RegistrationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => RegisterCubit(getIt.get<UserRepositoryInterface>()),
      child: RegistrationView(),
    );
  }
}

class RegistrationView extends StatelessWidget {
  RegistrationView({Key? key}) : super(key: key);

  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final bool processingRegister = false;
  final String registerError = '';

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterCubit, RegisterState>(
      listener: (BuildContext context, state) {
        if (state == RegisterState.success) {
          Navigator.pushNamedAndRemoveUntil(
              context, HomePage.name, (Route<dynamic> route) => false);
        }
      },
      child: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            title: const Text(
              'Faça seu cadastro',
              style: TextStyle(fontSize: 18),
            ),
          ),
          body: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                EditedTextFormField(text: 'Nome', controller: _nameController),
                EditedTextFormField(
                    text: 'Email', controller: _emailController),
                EditedTextFormField(
                    text: 'Senha', controller: _passwordController),
                const SizedBox(height: 20.0),
                BlocBuilder<RegisterCubit, RegisterState>(
                    builder: (context, state) {
                  return Visibility(
                    visible: state == RegisterState.registerFailed ||
                        state == RegisterState.userAlreadyExist,
                    child: Text(
                      state == RegisterState.userAlreadyExist
                          ? 'Usuário já cadastrado'
                          : 'Falha ao realizar cadastro',
                      style: const TextStyle(color: Colors.red),
                    ),
                  );
                }),
                BlocBuilder<RegisterCubit, RegisterState>(
                    builder: (context, state) {
                  return LoadingButton(
                    isLoading: state == RegisterState.processingRegister,
                    onPressed: () {
                      context.read<RegisterCubit>().register(
                          _nameController.text,
                          _emailController.text,
                          _passwordController.text);
                    },
                    child: const Text('Cadastrar'),
                  );
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
