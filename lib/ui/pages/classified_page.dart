import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/repositories/title_repository.dart';
import 'package:my_movies_list/ui/widgets/thumbnail.dart';

class ClassifiedPage extends StatelessWidget {
  static const name = 'classified-page';
  final double itemHeight = 100;
  final double itemWidth = 10;

  ClassifiedPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final titles = [];
    return Scaffold(
        appBar: AppBar(
          title: const Text('Classified'),
        ),
        body: ListView.builder(
          itemCount: titles.length,
          itemBuilder: (context, index) {
            return _buildTitleCard(titles[index], context);
          },
        ));
  }

  Widget _buildTitleCard(TitleModel title, BuildContext context) {
    return Container(
      alignment: Alignment.topLeft,
      child: Padding(
        padding: const EdgeInsets.only(right: 50, left: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Thumbnail(
              title: TitleThumbnailModel(
                  name: title.name, imageUrl: title.posterUrl, id: title.id),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  title.name,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
                const SizedBox(height: 20.0),
                Row(
                  children: const [
                    Chip(label: Text('Ação')),
                    SizedBox(width: 10.0),
                    Chip(label: Text('Aventura')),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(
                        onPressed: () {},
                        icon: const Icon(Icons.thumb_up_alt_outlined)),
                    IconButton(
                        onPressed: () {}, icon: const Icon(Icons.thumb_down)),
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
