import 'package:flutter/material.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/models/user_model.dart';
import 'package:my_movies_list/data/repositories/title_repository.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/ui/widgets/custom_circle_avatar.dart';
import 'package:my_movies_list/ui/widgets/thumbnail.dart';

import '../../locator.dart';

class UsersPage extends StatefulWidget {
  static const name = 'users-page';
  final _repository = getIt.get<UserRepositoryInterface>();
  UsersPage({Key? key}) : super(key: key);
  @override
  State<UsersPage> createState() => _UsersPage();
}

class _UsersPage extends State<UsersPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Contatos')),
      body: FutureBuilder<List<UserModel?>>(
        future: widget._repository.getUsers(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const CircularProgressIndicator();
          }

          if (!snapshot.hasData) {
            return const Text('Falha o carregar contatos');
          }

          final users = snapshot.data!;

          return GridView.count(
            padding: const EdgeInsets.only(top: 8.0),
            mainAxisSpacing: 0.0,
            childAspectRatio: 8,
            crossAxisCount: 1,
            children: users.map((t) => _buildUser(t!, context)).toList(),
          );
        },
      ),
    );
  }

  Widget _buildUser(UserModel user, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          CustomCircleAvatar(
            initials: user.name[0].toUpperCase(),
          ),
          SizedBox(width: 10),
          Text(
            user.name,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
          ),
        ],
      ),
    );
  }
}
