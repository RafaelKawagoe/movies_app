import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/blocs/login_cubit.dart';
import 'package:my_movies_list/data/exceptions/user_not_found.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/ui/pages/home_page.dart';
import 'package:my_movies_list/ui/pages/registration_page.dart';
import 'package:my_movies_list/ui/widgets/loading_button.dart';
import 'package:my_movies_list/ui/widgets/text_form_field.dart';

import '../../locator.dart';

class LoginPage extends StatelessWidget {
  static const name = 'login-page';

  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => LoginCubit(getIt.get<UserRepositoryInterface>()),
      child: LoginView(),
    );
  }
}

class LoginView extends StatelessWidget {
  final _userRepository = getIt.get<UserRepositoryInterface>();
  static const name = 'login-page';

  LoginView({Key? key}) : super(key: key);

  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  bool processingLogin = false;
  String loginError = '';

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginCubit, LoginState>(
      listener: (BuildContext context, state) {
        if (state == LoginState.success) {
          Navigator.pushReplacementNamed(context, HomePage.name);
        }
      },
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    'Informe suas credencias para começar!',
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.w300,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  EditedTextFormField(
                    text: 'Email',
                    keyBoardType: TextInputType.emailAddress,
                    controller: _emailController,
                  ),
                  EditedTextFormField(
                    text: 'Senha',
                    keyBoardType: TextInputType.emailAddress,
                    obscureText: true,
                    controller: _passwordController,
                  ),
                  const SizedBox(height: 20.0),
                  BlocBuilder<LoginCubit, LoginState>(
                      builder: (context, state) {
                    return Visibility(
                      visible: state == LoginState.loginFailed ||
                          state == LoginState.userNotFound,
                      child: Text(
                        state == LoginState.userNotFound
                            ? 'Usuário não encontrado'
                            : 'Falha ao realizar login',
                        style: const TextStyle(color: Colors.red),
                      ),
                    );
                  }),
                  BlocBuilder<LoginCubit, LoginState>(
                      builder: (context, state) {
                    return LoadingButton(
                      isLoading: state == LoginState.processingLogin,
                      onPressed: () {
                        context.read<LoginCubit>().login(
                            _emailController.text, _passwordController.text);
                      },
                      child: const Text('Entrar'),
                    );
                  }),
                  const SizedBox(height: 15.0),
                  Text(
                    'OU',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 12.0,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, RegistrationPage.name);
                    },
                    child: const Text('Criar minha conta'),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
