import 'package:flutter/material.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/home_page.dart';
import 'package:my_movies_list/ui/pages/login_page.dart';

class SplashPage extends StatefulWidget {
  final _userRepository = getIt.get<UserRepositoryInterface>();
  static const name = 'splash-page';

  SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();

    checkSession();
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Icon(
          Icons.movie,
          size: 30.0,
        ),
      ),
    );
  }

  Future<void> checkSession() async {
    await Future.delayed(const Duration(seconds: 2));
    var isLogged = await widget._userRepository.isLogged();

    if (isLogged) {
      Navigator.pushReplacementNamed(context, HomePage.name);
    } else {
      Navigator.pushReplacementNamed(context, LoginPage.name);
    }
  }
}
