import 'package:flutter/material.dart';
import 'package:my_movies_list/data/models/title_details_model.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/widgets/custom_network_image.dart';
import 'package:my_movies_list/ui/widgets/like_buttons.dart';
import 'package:my_movies_list/ui/widgets/loading_button.dart';
import 'package:my_movies_list/ui/widgets/text_form_field.dart';
import 'package:my_movies_list/ui/widgets/thumbnail.dart';
import 'package:my_movies_list/ui/widgets/title_carousel.dart';

class TitleDetailsPage extends StatefulWidget {
  static const name = 'title-details-page';

  final _repository = getIt.get<TitleRepositoryInterface>();
  final _commentController = TextEditingController();

  TitleDetailsPage({Key? key}) : super(key: key);
  @override
  State<TitleDetailsPage> createState() => _TitleDetailsPage();
}

class _TitleDetailsPage extends State<TitleDetailsPage> {
  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context)!.settings.arguments as Map;
    int titleId = arguments['id'] as int;
    bool isTvShow = arguments['isTvShow'] as bool;

    return Scaffold(
      body: SafeArea(
        child: FutureBuilder<TitleDetailModel?>(
          future:
              widget._repository.getTitleDetails(titleId, isTvShow: isTvShow),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const CircularProgressIndicator();
            }

            if (!snapshot.hasData) {
              return const Text('Falha o carregar detalhes');
            }

            final title = snapshot.data!;

            return SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomNetworkImage(url: title.coverUrl),
                  Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: _buildDetails(title, isTvShow),
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _buildDetails(TitleDetailModel title, bool isTvShow) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title.name +
              (title.releaseDate == null ? '' : '(${title.releaseDate!.year})'),
          style: const TextStyle(fontWeight: FontWeight.w500, fontSize: 18.0),
        ),
        const SizedBox(height: 20.0),
        const Text(
          'Sinopse:',
          style: TextStyle(
            fontWeight: FontWeight.w700,
          ),
        ),
        const SizedBox(height: 5.0),
        Text(
          title.overview,
          style: const TextStyle(fontSize: 15.0),
        ),
        const SizedBox(height: 10.0),
        Text('Duração: ${title.runtime} min'),
        const SizedBox(height: 20.0),
        Wrap(
          children: title.genres
              .map((g) => Padding(
                    padding: const EdgeInsets.only(left: 4.0),
                    child: Chip(label: Text(g)),
                  ))
              .toList(),
        ),
        LikeButton(
          value: false,
          onChanged: (e) {
            widget._repository.saveRate(title.id, e ? 1 : -1);
          },
        ),
        FutureBuilder<List<TitleModel>>(
          future: widget._repository
              .getTitleRecommendations(title.id, isTvShow: isTvShow),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const CircularProgressIndicator();
            }

            if (snapshot.data!.isNotEmpty) {
              final movies = snapshot.data!;
              return TitleCarousel(
                label: '',
                children: movies
                    .map((t) => _buildTitleCard(title: t, context: context))
                    .toList(),
              );
            }

            return Container();
          },
        ),
        const Text(
          'Comentários',
          style: TextStyle(
            fontSize: 18.0,
          ),
        ),
        const SizedBox(height: 15.0),
        Row(
          children: [
            Expanded(
              child: EditedTextFormField(
                text: 'Comentar',
                controller: widget._commentController,
              ),
            ),
            LoadingButton(
              child: const Text('Enviar'),
              onPressed: () async {
                await widget._repository.saveComment(
                  title.id,
                  widget._commentController.text,
                  isTvShow: isTvShow,
                );
                widget._commentController.clear();
                setState(() {});
              },
            ),
          ],
        ),
        ..._buildCommentsList(title)
      ],
    );
  }

  List<Widget> _buildCommentsList(TitleDetailModel title) {
    return title.comments
        .map((c) => ListTile(
              title: Text(c.text),
              subtitle: Text(c.date.toString()),
              trailing: IconButton(
                onPressed: () async {
                  await widget._repository
                      .removeComment(title.id, c.id, isTvShow: title.isTvShow);
                  setState(() {});
                },
                icon: const Icon(Icons.remove),
              ),
            ))
        .toList();
  }

  Widget _buildTitleCard(
      {required TitleModel title, required BuildContext context}) {
    return GestureDetector(
      child: GestureDetector(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Thumbnail(
            width: 120.0,
            title: TitleThumbnailModel(
                name: title.name, imageUrl: title.posterUrl, id: title.id),
          ),
        ),
      ),
    );
  }
}
