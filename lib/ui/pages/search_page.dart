import 'package:flutter/material.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/repositories/title_repository.dart';
import 'package:my_movies_list/ui/widgets/thumbnail.dart';

class SearchPage extends StatelessWidget {
  static const name = 'search-page';

  SearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TextFormField(
          style: const TextStyle(color: Colors.white),
          decoration: const InputDecoration(
            hintText: 'Pesquise aqui...',
            hintStyle: TextStyle(color: Colors.white30),
            border: InputBorder.none,
          ),
        ),
      ),
      body: GridView.count(
          padding: const EdgeInsets.only(top: 8.0),
          mainAxisSpacing: 20.0,
          crossAxisCount: 2,
          children: []),
    );
  }

  Widget _buildTitleCard(TitleModel title, BuildContext context) {
    return Center(
      child: Thumbnail(
          height: 160,
          title: TitleThumbnailModel(
              name: title.name, imageUrl: title.posterUrl, id: title.id)),
    );
  }
}
