import 'package:flutter/material.dart';

class LikeButton extends StatelessWidget {
  final bool value;
  final Function(bool)? onChanged;

  const LikeButton({
    required this.value,
    this.onChanged,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          onPressed: () {
            if (onChanged != null) {
              onChanged!(true);
            }
          },
          icon: Icon(
            value ? Icons.thumb_up : Icons.thumb_up_alt_outlined,
          ),
        ),
        IconButton(
          onPressed: () {
            if (onChanged != null) {
              onChanged!(false);
            }
          },
          icon: Icon(
            value ? Icons.thumb_down_alt_outlined : Icons.thumb_down,
          ),
        ),
      ],
    );
  }
}
