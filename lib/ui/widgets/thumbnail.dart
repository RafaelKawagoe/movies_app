import 'package:flutter/material.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:my_movies_list/ui/pages/title_details_page.dart';

class TitleThumbnailModel {
  String name;
  String imageUrl;
  int id;

  TitleThumbnailModel(
      {required this.name, required this.imageUrl, required this.id});
}

class Thumbnail extends StatelessWidget {
  final TitleThumbnailModel title;
  final Color backgroudnColor;
  final double? width;
  final double? height;

  const Thumbnail({
    required this.title,
    this.width = 160,
    this.height,
    this.backgroudnColor = Colors.blue,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 10),
      color: Colors.white,
      width: 120,
      height: 180,
      child: Stack(children: [
        Image.network(
          title.imageUrl,
          loadingBuilder: (context, child, loadingProgress) {
            if (loadingProgress == null) return child;

            return const Center(
                child: SpinKitFadingCircle(
              color: Colors.black,
              size: 30.0,
            ));
          },
        ),
        Container(
          alignment: Alignment.bottomLeft,
          margin: const EdgeInsets.all(3),
          child: const Text(
            '2015',
            style: TextStyle(
              color: Colors.white,
              shadows: <Shadow>[
                Shadow(
                  offset: Offset(0.5, 0.5),
                  blurRadius: 5.0,
                  color: Colors.black,
                ),
                Shadow(
                  offset: Offset(0.5, 0.5),
                  blurRadius: 10.0,
                  color: Colors.black38,
                ),
              ],
            ),
          ),
        ),
      ]),
    );
  }
}
