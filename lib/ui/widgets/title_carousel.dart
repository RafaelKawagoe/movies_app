import 'package:flutter/material.dart';

class TitleCarousel extends StatelessWidget {
  final String? label;
  final List<Widget> children;

  const TitleCarousel({
    this.label,
    required this.children,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label ?? '',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w700,
            color: Theme.of(context).primaryColorDark,
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Wrap(
            spacing: 10,
            children: children,
          ),
        )
      ],
    );
  }
}
