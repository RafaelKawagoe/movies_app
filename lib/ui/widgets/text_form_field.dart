import 'package:flutter/material.dart';

class EditedTextFormField extends StatelessWidget {
  final String text;
  final TextEditingController? controller;
  final bool obscureText;
  final TextInputType? keyBoardType;

  const EditedTextFormField({
    required this.text,
    this.controller,
    this.obscureText = false,
    this.keyBoardType,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 25.0),
        TextFormField(
          keyboardType: keyBoardType,
          controller: controller,
          obscureText: obscureText,
          decoration: InputDecoration(
            labelText: text,
            border: OutlineInputBorder(),
          ),
        ),
      ],
    );
  }
}
