import 'package:flutter/material.dart';

class CustomCircleAvatar extends StatefulWidget {
  // NetworkImage myImage;

  String initials;

  CustomCircleAvatar({
    // this.myImage,
    required this.initials,
    Key? key,
  }) : super(key: key);

  @override
  _CustomCircleAvatarState createState() => new _CustomCircleAvatarState();
}

class _CustomCircleAvatarState extends State<CustomCircleAvatar> {
  @override
  Widget build(BuildContext context) {
    return CircleAvatar(child: new Text(widget.initials));
  }
}
