import 'package:flutter/material.dart';

class TitleRowModel {
  num genre;
  String title;
  BuildContext context;
  bool isTvShow;

  TitleRowModel(
      {required this.genre,
      required this.title,
      required this.context,
      this.isTvShow = true});

  factory TitleRowModel.fromjson(Map<String, dynamic> json) {
    return TitleRowModel(
      genre: json['genre'],
      title: json['title'],
      context: json['context'],
      isTvShow: json['isTvShow'],
    );
  }
}
