import 'package:my_movies_list/data/models/title_details_model.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';
import 'package:my_movies_list/data/repositories/user_repository.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/data/services/http_service.dart';

import '../../locator.dart';

class TitleRepository implements TitleRepositoryInterface {
  final HttpService _httpService;
  final _baseUrl = 'https://xbfuvqstcb.execute-api.us-east-1.amazonaws.com/dev';
  final _userRepository = getIt.get<UserRepositoryInterface>();

  TitleRepository(this._httpService);

  @override
  Future<List<TitleModel>> getMovieList({Map<String, dynamic>? params}) async {
    String uri = '$_baseUrl/movies';
    var response = await _httpService.getRequest(uri, params: params);
    if (response.success) {
      List<dynamic> data = response.content!['data'];

      return List<TitleModel>.from(data.map((j) => TitleModel.fromjson(j)));
    }

    return [];
  }

  @override
  Future<List<TitleModel>> getTitleRecommendations(num id,
      {bool isTvShow = false}) async {
    final titleType = isTvShow ? 'tv' : 'movies';
    final uri = '$_baseUrl/$titleType/$id/recommendations';

    var response = await _httpService.getRequest(uri);
    if (response.success) {
      List<dynamic> data = response.content!['data'];

      return List<TitleModel>.from(data.map((j) => TitleModel.fromjson(j)));
    }

    return [];
  }

  @override
  Future<TitleDetailModel?> getTitleDetails(int id,
      {bool isTvShow = false}) async {
    final titleType = isTvShow ? 'tv' : 'movies';
    final uri = '$_baseUrl/$titleType/$id';
    var response = await _httpService.getRequest(uri);
    if (response.success) {
      var title = TitleDetailModel.fromJson(response.content!);
      title.isTvShow = isTvShow;
      return title;
    }

    return null;
  }

  @override
  Future<List<TitleModel>> getTvList({Map<String, dynamic>? params}) async {
    String uri = '$_baseUrl/tv';
    var response = await _httpService.getRequest(uri, params: params);
    if (response.success) {
      List<dynamic> data = response.content!['data'];

      return List<TitleModel>.from(data.map((j) => TitleModel.fromjson(j)));
    }
    return [];
  }

  @override
  Future<bool> removeComment(num titleId, num commentId,
      {bool isTvShow = false}) async {
    final titleType = isTvShow ? 'tv' : 'movies';
    final token = await _userRepository.getToken();
    final uri = '$_baseUrl/$titleType/$titleId/$commentId/comment';
    final result = await _httpService.deleteRequest(uri, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });

    return result.success;
  }

  @override
  Future<bool> saveComment(num titleId, String text,
      {bool isTvShow = false}) async {
    final titleType = isTvShow ? 'tv' : 'movies';
    final uri = '$_baseUrl/$titleType/$titleId/comment';
    final token = await _userRepository.getToken();
    final result = await _httpService.postRequest(uri, {
      'comment': text
    }, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });

    return result.success;
  }

  @override
  Future<bool> saveRate(num titleId, num rate, {bool isTvShow = false}) async {
    final titleType = isTvShow ? 'tv' : 'movies';
    final uri = '$_baseUrl/$titleType/$titleId/rate';
    final token = await _userRepository.getToken();
    final result = await _httpService.postRequest(uri, {
      'rate': rate
    }, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });

    return result.success;
  }

  @override
  Future<List<TitleModel>> getTitlesRated() async {
    String uri = '$_baseUrl/users/titles-rated';
    final token = await _userRepository.getToken();
    var response = await _httpService.getRequest(uri, params: {
      'rate': 1
    }, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });
    if (response.success) {
      List<dynamic> data = response.content!['data'];

      return List<TitleModel>.from(data.map((j) => TitleModel.fromjson(j)));
    }

    return [];
  }

  @override
  Future<List<CommentModel>> getComments(num titleId, bool isTvShow) async {
    final titleType = isTvShow ? 'tv' : 'movies';
    final uri = '$_baseUrl/$titleType/$titleId/comments';
    final token = await _userRepository.getToken();
    var response = await _httpService.getRequest(uri, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });
    if (response.success) {
      List<dynamic> data = response.content!['data'];

      return List<CommentModel>.from(data.map((j) => CommentModel.fromJson(j)));
    }

    return [];
  }
}
