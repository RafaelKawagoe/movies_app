import 'package:my_movies_list/data/models/user_model.dart';

abstract class UserRepositoryInterface {
  Future<UserModel?> login(String email, String password);

  Future<UserModel?> getUser();

  Future<List<UserModel?>> getUsers();

  Future<UserModel?> register(String name, String email, String password);

  Future<void> saveToken(String token);

  Future<String?> getToken();

  Future<bool> isLogged();

  Future<void> clearSession();
}
