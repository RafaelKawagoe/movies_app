import 'package:my_movies_list/data/models/title_details_model.dart';
import 'package:my_movies_list/data/models/title_model.dart';

abstract class TitleRepositoryInterface {
  Future<List<TitleModel>> getMovieList({Map<String, dynamic>? params});

  Future<TitleDetailModel?> getTitleDetails(int id, {bool isTvShow = false});

  Future<List<TitleModel>> getTvList({Map<String, dynamic>? params});

  Future<List<TitleModel>> getTitleRecommendations(num id,
      {bool isTvShow = false});

  Future<bool> saveComment(num titleId, String text, {bool isTvShow = false});

  Future<bool> removeComment(num titleId, num commentId,
      {bool isTvShow = false});

  Future<bool> saveRate(num titleId, num rate, {bool isTvShow = false});

  Future<List<TitleModel>> getTitlesRated();

  Future<List<CommentModel>> getComments(num id, bool isTvShow);
}
